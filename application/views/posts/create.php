<h2><?= $title ?></h2>

<?php echo validation_errors(); ?>

<?php echo form_open('posts/create'); ?>
  <div class="form-group">
    <label>Title</label>
    <input type="text" name="title" placeholder="Add Title" class="form-control">
  </div>
  <div class="form-group">
    <label>Body</label>
    <textarea class="form-control" name="body" placeholder="Write post here."></textarea>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>