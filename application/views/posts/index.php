<h2><?= $title ?></h2>
<?php foreach($posts as $post) : ?>
    <h3><a href="<?php echo site_url('/posts/'.$post['slug']); ?>"><?php echo $post['title']; ?></a></h3>
    <small class="post-date">Posted on: <?php echo $post['created_at']; ?></small><br/>
    <?php echo $post['body']; ?>
    <br/><br/>
    <p><a class="btn btn-default" href="<?php echo site_url('/posts/'.$post['slug']); ?>">Read More</a></p>
<?php endforeach; ?>